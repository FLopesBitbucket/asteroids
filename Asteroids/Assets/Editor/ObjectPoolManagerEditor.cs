using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(ObjectPoolManager))]
public class ObjectPoolManagerEditor : Editor {

	private ObjectPoolManager baseScript;
	private PoolObject[] items;
	private PoolObject _item = new PoolObject();
	private int selectedIndex = -1;
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		baseScript = (ObjectPoolManager)target;
		items = baseScript.objectList;
		
		if (GUILayout.Button ("Create/Update Enum", GUILayout.Height (20), GUILayout.ExpandWidth (true))) {
			this.CreateEnum ();
		}
		GUILayout.Space (40);
		
		this.DrawProperties ();
		this.DrawList ();
	}
	
	private void DrawProperties ()
	{
        _item.poolObjectName = EditorGUILayout.TextField("Pool Object Name", _item.poolObjectName);
        _item.prefab = (GameObject)EditorGUILayout.ObjectField ("Prefab", _item.prefab, typeof(GameObject), false);
		_item.poolStartSize = EditorGUILayout.IntField ("Pool Start Size", _item.poolStartSize);
		_item.poolStep = EditorGUILayout.IntField ("Pool Step", _item.poolStep);
		
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Apply", GUILayout.ExpandWidth (true), GUILayout.Height (20))) {
			this.ApplyChanges ();
		}
		if (GUILayout.Button ("Add New", GUILayout.ExpandWidth (true), GUILayout.Height (20))) {
			this.AddItem ();
		}
		if (GUILayout.Button ("Reset", GUILayout.ExpandWidth (true), GUILayout.Height (20))) {
			this.ResetItem ();
		}
		GUILayout.EndHorizontal ();
	}
	
	private void DrawList ()
	{
		GUILayout.Space (45);
		
		GUILayout.Label ("Prefabs List");
		if (items != null) {
			for (int i = 0; i < items.Length; i++) {
				GUILayout.Space (5);
				
				GUILayout.BeginHorizontal ();
				
				GUILayout.Button ("", GUILayout.Width (50), GUILayout.Height (50));
				
				Rect a = GUILayoutUtility.GetLastRect ();
                //EditorGUI.DrawPreviewTexture (new Rect (a.x + 5, a.y + 5, 40, 40), AssetPreview.GetAssetPreview(items[i].prefab));
				
				GUILayout.BeginVertical ();

                GUILayout.Button("Name: " + items[i].poolObjectName, GUILayout.ExpandWidth(true), GUILayout.Height(25));

                if (items[i].prefab != null)
                {
                    GUILayout.Button("Prefab: " + items[i].prefab.name, GUILayout.ExpandWidth(true), GUILayout.Height(25));
                }else
                {
                    GUILayout.Label("ERROR - MISSING REFERENCE");
                }
                
                GUILayout.Space (2);
				GUILayout.BeginHorizontal ();
			
				if (GUILayout.Button ("Edit", GUILayout.ExpandWidth (true), GUILayout.Height (19))) {
					this.EditItem (i);
				}
				
				if (GUILayout.Button ("Remove", GUILayout.ExpandWidth (true), GUILayout.Height (19))) {
					this.RemoveItemDialog (i);
				}
				
				GUILayout.EndHorizontal ();
				
				GUILayout.EndVertical ();
				
				GUILayout.EndHorizontal ();
				
				GUILayout.Space (5);
			}
		}
	}
	
	
	private void RemoveItemDialog (int index)
	{
		if (EditorUtility.DisplayDialog ("Tem certeza?", "Deseja mesmo remover?", "Sim", "Nao")) {
			this.RemoveItem (index);
			
			this.ReorderItems ();
		}
	}
	
	private void AddItem ()
	{
		if (_item.prefab == null) {
			EditorUtility.DisplayDialog ("Erro", "Nao � possivel criar prefab sem GameObject!", "OK");
			return;
		}
		if (items == null)
			items = new PoolObject[0];
		List<PoolObject> list = new List<PoolObject>(items);
		list.Add (_item);
		items = list.ToArray ();
		
		_item = null;
		_item = new PoolObject();
		
		selectedIndex = -1;
		
		this.ReorderItems ();
	}
	
	private void RemoveItem (int index)
	{
		List<PoolObject> list = new List<PoolObject>(items);
		list.RemoveAt (index);
		baseScript.objectList = items = list.ToArray ();
		
		selectedIndex = -1;
	}
	
	private void EditItem (int index)
	{
        _item.poolObjectName = items[index].poolObjectName;
		_item.prefab = items [index].prefab;
		_item.poolStartSize = items [index].poolStartSize;
		_item.poolStep = items [index].poolStep;
		
		selectedIndex = index;
	}
	
	private void ResetItem ()
	{
        _item.poolObjectName = "";
		_item.prefab = null;
		_item.poolStartSize = 0;
		_item.poolStep = 0;
		selectedIndex = -1;
	}
	
	private void ApplyChanges ()
	{
		if (selectedIndex != -1) {

            items[selectedIndex].poolObjectName = _item.poolObjectName;
            items [selectedIndex].prefab = _item.prefab;
			items [selectedIndex].poolStartSize = _item.poolStartSize;
			items [selectedIndex].poolStep = _item.poolStep;
			
			this.ResetItem();
			
			this.ReorderItems ();
		}
	}
	
	private void ReorderItems ()
	{
		System.Array.Sort(items, delegate(PoolObject item1, PoolObject item2)
		{
			return item1.prefab.name.CompareTo (item2.prefab.name);
		});
		
		baseScript.objectList = items;
	}
	
	
	private void CreateEnum ()
	{
        string fileLocation = Application.dataPath + "/Scripts/Managers/ObjectPoolManager/PrefabsEnum.cs";

		string scriptText = "";
		if (File.Exists(fileLocation))
		{
			string fileContents = File.ReadAllText(fileLocation);

			string scriptPrefix;
			string scriptSuffix;

            //string sceneName = EditorApplication.currentScene.Remove(0, EditorApplication.currentScene.LastIndexOf('/') + 1).Replace(".unity", "");
            //if (fileContents.Contains("//" + sceneName))
            //{
            //    scriptPrefix = fileContents.Substring(0, fileContents.IndexOf("//" + sceneName));
            //    scriptSuffix = fileContents.Substring(fileContents.LastIndexOf("//" + sceneName) + ("//" + sceneName).Length);
            //}
            //else
			{
				scriptPrefix = fileContents.Substring(0, fileContents.IndexOf("//STARTENUM") + "//STARTENUM".Length);
				scriptSuffix = fileContents.Substring(fileContents.IndexOf("//STARTENUM") + "//STARTENUM".Length);
			}

			string sceneContents = "//"+/*sceneName+*/"\n";

			string poolObjectName;
			if (baseScript.objectList.Length > 0)
			{
				for (int i = 0; i < baseScript.objectList.Length; i++)
				{
                    if (!fileContents.Contains(baseScript.objectList[i].poolObjectName))
                    {
                        poolObjectName = "	" + baseScript.objectList[i].poolObjectName.Replace(" ", "_");
                        poolObjectName = poolObjectName.Replace("-", "_");
                        sceneContents += poolObjectName + " = " + i + ", \n";
                    }
				}
			}
            sceneContents += "//" /*+ sceneName*/;

			

			File.Delete(fileLocation);
						
			scriptText = scriptPrefix + sceneContents + scriptSuffix;

			System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
			byte[] data = encoding.GetBytes(scriptText);

			File.WriteAllBytes(fileLocation, data);

			AssetDatabase.Refresh();
		}
		else
		{
			Debug.LogError("Arquivo PrefabsEnum.cs n�o existe. Favor consegu�-lo!");
		}
	}
}
