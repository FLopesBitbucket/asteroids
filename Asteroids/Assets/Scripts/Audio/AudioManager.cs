﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public AudioPoolController audioController;
    protected ObjectPoolManager activePool;

    public PoolType poolType;
    public AudioComponent[] audioList;
    private int[] audioInstancesController;
      

    /// <summary>
    /// Tocar Som relacionado ao Index, permitindo alteração no pitch
    /// </summary>
    /// <param name="_index"></param>
    /// <param name="_pitch"></param>
    protected void PlayEvent(int _index, float _pitch)
    {
        if (audioInstancesController == null)
        {
            audioInstancesController = new int[audioList.Length];
        }

        if (audioList[_index].maxAudioInstances >= audioInstancesController[_index])
        {
            audioInstancesController[_index]++;

            AudioSource _audio = (AudioSource)activePool.Instantiate(audioList[_index].audioType, typeof(AudioSource));

            _audio.pitch = _pitch;
            _audio.Play();

            if (audioList[_index].activeGameObject == null)
                audioList[_index].activeGameObject = new Queue<GameObject>();

            audioList[_index].activeGameObject.Enqueue(_audio.gameObject);

            StartCoroutine(AudioCallbackEnd(_index, _audio.clip.length));
        }
        else
        {
            Debug.LogWarning("LIMITED UPGRADE YOUR CAP " + audioList[_index].audioType);
        }
    }

    /// <summary>
    /// Tocar Som relacionado ao Index
    /// </summary>
    /// <param name="_index"></param>
    protected void PlayEvent(int _index)
    {
        PlayEvent(_index, 1f);
    }

    /// <summary>
    /// Callback manual para saber quando o som acabou e reciclá-lo.
    /// </summary>
    /// <param name="_index"></param>
    /// <param name="_lenght"></param>
    /// <returns></returns>
    private IEnumerator AudioCallbackEnd (int _index, float _lenght)
    {
        yield return new WaitForSeconds(_lenght);

        audioInstancesController[_index]--;
        activePool.RecicleObject(audioList[_index].audioType, audioList[_index].activeGameObject.Dequeue());
    }

}
