﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioComponent {

    public PrefabsEnum audioType;
    public int maxAudioInstances;

    [HideInInspector]
    public Queue<GameObject> activeGameObject;
}
