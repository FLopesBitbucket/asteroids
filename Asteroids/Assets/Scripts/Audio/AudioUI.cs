﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioUI : AudioManager
{
    private MenuManager menumanager;
    private UIGameManager uigamemanager;

    void Awake()
    {
        menumanager = GameObject.FindObjectOfType<MenuManager>();
        uigamemanager = GameObject.FindObjectOfType<UIGameManager>();
    }

    void Start()
    {
        if (menumanager != null)
        {
            menumanager.OnBack += PlayBack;
            menumanager.OnClick += PlayClicked;
            menumanager.OnNavegation += PlayNavegation;
        }

        if (uigamemanager != null)
        {
            uigamemanager.OnBack += PlayBack;
            uigamemanager.OnClick += PlayClicked;
            uigamemanager.OnNavegation += PlayNavegation;
        }

        activePool = audioController.GetUIPool();
    }

    /// <summary>
    /// Som de click UI
    /// </summary>
    private void PlayClicked()
    {
        PlayEvent(0, 1f);
    }

    /// <summary>
    /// Som de navegação UI
    /// </summary>
    private void PlayNavegation()
    {
        PlayEvent(0, 0.75f);
    }

    /// <summary>
    /// Som de voltar UI
    /// </summary>
    private void PlayBack()
    {
        PlayEvent(0, 0.5f);
    }

    void OnDestroy()
    {
        if (menumanager != null)
        {
            menumanager.OnBack -= PlayBack;
            menumanager.OnClick -= PlayClicked;
            menumanager.OnNavegation -= PlayNavegation;
        }

        if (uigamemanager != null)
        {
            uigamemanager.OnBack -= PlayBack;
            uigamemanager.OnClick -= PlayClicked;
            uigamemanager.OnNavegation -= PlayNavegation;
        }
    }
}
