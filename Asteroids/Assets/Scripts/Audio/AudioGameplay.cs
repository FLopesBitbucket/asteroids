﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Gameplay order 0 - AudioAsteroidDamaged / 
//               1 - AudioShipHyperspace / 2 - AudioShipShoot / 3 - AudioShipDamaged 
//               4 - AudioEnemyShoot / 5 - AudioEnemyDamaged 
//               6 - AudioFlowStart / 7 - AudioFlowNewLevel / 8 - AudioFlowLose /
//               9 - ShieldUP / 10 - ShieldDown

public class AudioGameplay : AudioManager
{
    public PlayerShip player;
    public GameManager gamemanager;
    public EnemyShipManager enemyshipsmanager;
    private EnemyShip actualEnemy;

    void Start()
    {
        activePool = audioController.GetGameplayPool();

        player.OnShipHyperSpace += PlayShipHyperSpace;
        player.OnShipShoot += PlayShipShoot;
        player.OnShipDamaged += PlayShipDamaged;

        gamemanager.OnGameStart += PlayFlowStart;
        gamemanager.OnNewLevel += PlayFlowNewLevel;
        gamemanager.OnGameEnds += PlayFlowLose;

        gamemanager.OnDestroyAsteroid += PlayAsteroidDamaged;

        gamemanager.OnDestroyEnemy += PlayEnemyDamaged;
        enemyshipsmanager.OnSpawnNewEnemy += NewEnemySpawned;

        gamemanager.OnPickShield += PlayShieldUp;
        gamemanager.OnBreakShield += PlayShieldDown;
    }

    void OnDestroy()
    {
        player.OnShipHyperSpace -= PlayShipHyperSpace;
        player.OnShipShoot -= PlayShipShoot;
        player.OnShipDamaged -= PlayShipDamaged;

        gamemanager.OnGameStart -= PlayFlowStart;
        gamemanager.OnNewLevel -= PlayFlowNewLevel;
        gamemanager.OnGameEnds -= PlayFlowLose;

        gamemanager.OnDestroyAsteroid -= PlayAsteroidDamaged;

        gamemanager.OnDestroyEnemy -= PlayEnemyDamaged;
        enemyshipsmanager.OnSpawnNewEnemy -= NewEnemySpawned;

        gamemanager.OnPickShield -= PlayShieldUp;
        gamemanager.OnBreakShield -= PlayShieldDown;

        if (actualEnemy != null)
        {
            actualEnemy.OnShipShoot -= PlayEnemyShoot;
        }
    }

    #region Asteroid Sounds

    /// <summary>
    /// Som ao destruir asteroid
    /// </summary>
    /// <param name="_asteroid"></param>
    /// <param name="_point"></param>
    private void PlayAsteroidDamaged(GameObject _asteroid, Vector2 _point)
    {
        PlayEvent(0);
    }

    #endregion

    #region Player Ship Sounds

    /// <summary>
    /// Som ao entrar no hyperespaço
    /// </summary>
    private void PlayShipHyperSpace()
    {
        PlayEvent(1);
    }

    /// <summary>
    /// Som ao atirar
    /// </summary>
    private void PlayShipShoot()
    {
        PlayEvent(2);
    }

    /// <summary>
    /// Som ao sofrer dano
    /// </summary>
    private void PlayShipDamaged()
    {
        PlayEvent(3);
    }
    #endregion

    #region Enemy Ship Sounds

    /// <summary>
    /// Ao spawnar inimigo adicionar delegate OnShipShoot
    /// </summary>
    /// <param name="_ship"></param>
    private void NewEnemySpawned(EnemyShip _ship)
    {
        actualEnemy = _ship;
        actualEnemy.OnShipShoot += PlayEnemyShoot;
    }

    /// <summary>
    /// Som ao atirar
    /// </summary>
    private void PlayEnemyShoot()
    {
        PlayEvent(4);
    }

    /// <summary>
    /// Som ao sofrer danos
    /// </summary>
    /// <param name="_asteroid"></param>
    /// <param name="_point"></param>
    private void PlayEnemyDamaged(GameObject _asteroid, Vector2 _point)
    {
        actualEnemy.OnShipShoot -= PlayEnemyShoot;
        actualEnemy = null;

        PlayEvent(5);
    }
    #endregion

    #region GameFlow Sounds

    /// <summary>
    /// Som ao começar o jogo
    /// </summary>
    private void PlayFlowStart()
    {
        PlayEvent(6);
    }

    /// <summary>
    /// Som ao ir para o proximo level
    /// </summary>
    private void PlayFlowNewLevel()
    {
        PlayEvent(7);
    }

    /// <summary>
    /// Som ao perder
    /// </summary>
    private void PlayFlowLose()
    {
        PlayEvent(8);
    }
    #endregion

    #region Shield
    /// <summary>
    /// Som ao receber shield
    /// </summary>
    private void PlayShieldUp()
    {
        PlayEvent(9);
    }

    /// <summary>
    /// Som ao perder shield
    /// </summary>
    private void PlayShieldDown()
    {
        PlayEvent(10);
    }
    #endregion
}
