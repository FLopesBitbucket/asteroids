﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPoolController : MonoBehaviour {

    [Header("UI Pool")]
    public ObjectPoolManager uiPoolRef;
    private ObjectPoolManager uiPool;
    public bool isUiPoolActive;

    [Header("Gameplay Pool")]
    public ObjectPoolManager gameplayPoolRef;
    private ObjectPoolManager gameplayPool;
    public bool isGameplayPoolActive;

    void Awake()
    {
        if (isUiPoolActive)
        {
            uiPool = (ObjectPoolManager)Instantiate(uiPoolRef);
        }

        if (isGameplayPoolActive)
        {
            gameplayPool = (ObjectPoolManager)Instantiate(gameplayPoolRef);
        }
    }

    #region Getters
    /// <summary>
    /// Retorna pool com sons da UI
    /// </summary>
    /// <returns></returns>
    public ObjectPoolManager GetUIPool()
    {
        return uiPool;
    }

    /// <summary>
    /// Retorna pool com sons da gameplay
    /// </summary>
    /// <returns></returns>
    public ObjectPoolManager GetGameplayPool()
    {
        return gameplayPool;
    }
    #endregion

}
