﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Para implementar um joystick virtual basta substituir os valores das variaveis dessa classe.
/// Utilizar o método VirtualInput para isso.
public class CustomInput : MonoBehaviour {

    public bool enableVirtualInput = false;

    //Axis Values
    private float horizontalSpeed;
    private float verticalSpeed;

    private readonly string s_Horizontal = "Horizontal";
    private readonly string s_Vertical = "Vertical";

    //Btns
    private bool ActionA = false;
    private bool ActionB;
    private bool ActionX;
    private bool ActionY;
    private bool ActionStart;

    private readonly string s_ActionA = "ActionA";
    private readonly string s_ActionB = "ActionB";
    private readonly string s_ActionX = "ActionX";
    private readonly string s_ActionY = "ActionY";
    private readonly string s_ActionStart = "ActionStart";
    		
	void Update () 
    {
        if (!enableVirtualInput)
        {
            DefaultInput();
        }       
	}


    #region Getters
    
    public float GetHorizontalSpeed()
    {
        return horizontalSpeed;
    }

    public float GetVerticalSpeed()
    {
        return verticalSpeed;
    }

    public bool GetActionA()
    {
        return ActionA;
    }

    public bool GetActionB()
    {
        return ActionB;
    }

    public bool GetActionX()
    {
        return ActionX;
    }

    public bool GetActionY()
    {
        return ActionY;
    }

    public bool GetActionStart()
    {
        return ActionStart;
    }
    #endregion

    #region DefaultInput

    void DefaultInput()
    {
        horizontalSpeed = Input.GetAxis(s_Horizontal);
        verticalSpeed = Input.GetAxis(s_Vertical);

        ActionA = Input.GetButtonDown(s_ActionA);
        ActionB = Input.GetButtonDown(s_ActionB);
        ActionX = Input.GetButtonDown(s_ActionX);
        ActionY = Input.GetButtonDown(s_ActionY);

        ActionStart = Input.GetButtonDown(s_ActionStart);  

    }

    #endregion

    #region VirtualInput
   
    void VirtualInput()
    {

    }
    #endregion

   

   
}
