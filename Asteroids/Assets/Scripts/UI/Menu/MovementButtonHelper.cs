﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MovementButtonHelper : MonoBehaviour
{
    private MenuManager menumanager;

    [SerializeField] private CustomMoveData[] customMoveAction;
  
    void Awake()
    {
        menumanager = FindObjectOfType<MenuManager>();
    }

    void Start()
    {
        EventTrigger trigger = GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Move;
        entry.callback.AddListener((data) => { OnMove((AxisEventData)data); });
        trigger.triggers.Add(entry);
    }

    public void OnMove(AxisEventData eventData)
    {
        for (int i = 0; i < customMoveAction.Length; i++)
        {
            if (customMoveAction[i].direction == eventData.moveDir)
            {
                if (customMoveAction[i].action != null)
                {
                    customMoveAction[i].action.Invoke();
                }             
            }    
        }        
    }  
}

[System.Serializable]
public struct CustomMoveData
{
    public MoveDirection direction;
    public UnityEvent action;
}