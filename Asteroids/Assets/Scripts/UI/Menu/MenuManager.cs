﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    private GameObject lastselect;

    public GameObject mainContainer;
    public GameObject playModeContainer;
    public GameObject leaderboardContainer;
    public GameObject colorsContainer;
    public GameObject quitContainer;
    public GameObject gamemodeContainer;

    public GameObject classicGameModeDefaultBtn;
    public GameObject playModeNavegationDefaultBtn;
    public GameObject leaderboardNavegationDefaultBtn;
    public GameObject blueColorBtnNavegationDefaultBtn;
    public GameObject colorsNavegationDefaultBtn;
    public EventSystem currentEventSystem;

    [Header("Ship Color Icons")]
    public Image[] ships;

    public Text[] scoreBoard;

    private int a_lastscorefx = Animator.StringToHash("lastscorefx");

    void Awake()
    {
#if !UNITY_EDITOR
        Cursor.visible = false;
#endif 
    }

    void Update()
    {
        if (currentEventSystem.currentSelectedGameObject == null)
        {
            currentEventSystem.SetSelectedGameObject(lastselect);
        }
        else
        {
            lastselect = currentEventSystem.currentSelectedGameObject;
        }
    }

    #region Fires and Delegates

    public delegate void NavegationEvents();
    public event NavegationEvents OnClick;
    public event NavegationEvents OnNavegation;
    public event NavegationEvents OnBack;

    private void FireOnClick()
    {
        if (OnClick != null)
        {
            OnClick();
        }
    }

    public void FireOnNavegation()
    {
        if (OnNavegation != null)
        {
            OnNavegation();
        }
    }

    private void FireOnBack()
    {
        if (OnBack != null)
        {
            OnBack();
        }
    }

    #endregion          
    
    #region LeaderboardContainer
    /// <summary>
    /// Inicializa resultados da leaderboard
    /// </summary>
    void OpenLeaderboard()
    {
        var _data = FlowControl.Instance.GetData();

        Dictionary<string, LeaderboardComponent> _dict = _data.Load();

        int _lastscore = FlowControl.Instance.GetLastScore();
        bool lastScoreFX = false;

        for (int i = 0; i < scoreBoard.Length; i++)
        {
            scoreBoard[i].text = _dict[_data.s_scoreKey + i].name + "  -  " + _dict[_data.s_scoreKey + i].score;

            if (_lastscore == _dict[_data.s_scoreKey + i].score && !lastScoreFX)
            {
                lastScoreFX = true;
                scoreBoard[i].GetComponent<Animator>().SetTrigger(a_lastscorefx);
            }
        }
    }

    /// <summary>
    /// Retorna ao modo de seleçao de partida
    /// </summary>
    public void BackToPlayModeContainer()
    {
        leaderboardContainer.gameObject.SetActive(false);       
        currentEventSystem.SetSelectedGameObject(playModeNavegationDefaultBtn);
        lastselect = playModeNavegationDefaultBtn;

        FireOnBack();
    }
    #endregion

    #region PlayModeContainer
    /// <summary>
    /// Jogar =)
    /// </summary>
    public void PlayGame()
    {
        FireOnClick();

        FlowControl.Instance.LoadGameplay();
    }

    /// <summary>
    /// abrir submenu de gamemods
    /// </summary>
    public void ShowGameModes()
    {
        FireOnClick();

        gamemodeContainer.gameObject.SetActive(true);
        gamemodeContainer.GetComponent<CanvasGroup>().alpha = 1;
        currentEventSystem.SetSelectedGameObject(classicGameModeDefaultBtn);
        lastselect = classicGameModeDefaultBtn;
    }

    public void HideGameModes()
    {
        FireOnBack();

        gamemodeContainer.GetComponent<CanvasGroup>().alpha = 0.2f;

        currentEventSystem.SetSelectedGameObject(playModeNavegationDefaultBtn);
        lastselect = playModeNavegationDefaultBtn;
    }

    /// <summary>
    /// abrir página de leaderboard
    /// </summary>
    public void ShowLeaderBoard()
    {
        FireOnClick();
   
        OpenLeaderboard();
    }

    /// <summary>
    /// Abrir menu de cores
    /// </summary>
    public void OpenColors()
    {
        colorsContainer.gameObject.SetActive(true);

        colorsContainer.GetComponentInChildren<CanvasGroup>().alpha = 1;

        currentEventSystem.SetSelectedGameObject(blueColorBtnNavegationDefaultBtn);
        lastselect = blueColorBtnNavegationDefaultBtn;

        HighlightChoosedShip(FlowControl.Instance.GetGamePressets().GetHashCode());
    }

    /// <summary>
    /// Voltar ao menu principal
    /// </summary>
    public void BackToMainContainer()
    {
        mainContainer.gameObject.SetActive(true);

        colorsContainer.GetComponentInChildren<CanvasGroup>().alpha = 0.2f;

        currentEventSystem.SetSelectedGameObject(colorsNavegationDefaultBtn);
        lastselect = colorsNavegationDefaultBtn;
        FireOnBack();
    }

    #endregion

    #region MainContainer

    /// <summary>
    /// Abrir créditos
    /// </summary>
    public void QuitCredits()
    {
        mainContainer.gameObject.SetActive(false);
        quitContainer.SetActive(true);

        Invoke("Quit", 7f);
    }

    /// <summary>
    /// sair
    /// </summary>
    void Quit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Setar GameMode Classic
    /// </summary>
    public void SetGameModeClassic()
    {
        FireOnClick();

        SetGameMode(GameModes.classic);        
    }

    /// <summary>
    /// Setar GameModeAdvanced
    /// </summary>
    public void SetGameModeAdvanced()
    {
        FireOnClick();

        SetGameMode(GameModes.advanced);
    }

    /// <summary>
    /// Enviar informaçao de gamemode para a flowcontrol
    /// </summary>
    /// <param name="_gamemode"></param>
    private void SetGameMode(GameModes _gamemode)
    {
        FlowControl.Instance.SetGameMode(_gamemode);

        PlayGame();
    }

    #endregion

    #region Colors
    /// <summary>
    /// Setar cor dos pressets
    /// 0 - blue
    /// 1 - red
    /// 2 - green
    /// 3 - orange
    /// </summary>
    /// <param name="_index"></param>
    public void SetColor(int _index)
    {
        GamePresets _pressets = GamePresets.blue;

        if (_index == 0)
        {
            _pressets = GamePresets.blue;
        }
        else if (_index == 1)
        {
            _pressets = GamePresets.red;
        }
        else if (_index == 2)
        {
            _pressets = GamePresets.green;
        }
        else if (_index == 3)
        {
            _pressets = GamePresets.orange;
        }

        FlowControl.Instance.SetGamePressets(_pressets);
        HighlightChoosedShip(_index);

    }

    void HighlightChoosedShip(int _index)
    {
        for (int i = 0; i < ships.Length; i++)
        {
            if (i == _index)
            {
                ships[i].color = new Vector4(1, 1, 1, 1);
            }
            else
            {
                ships[i].color = new Vector4(1, 1, 1, 0.2f);
            }
        }
    }
    #endregion

}
