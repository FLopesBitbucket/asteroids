﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleShipMenu : MonoBehaviour, IBoundary 
{

    [Header("Screen Boundary Variables")]
    private float cameraDistance;
    private Vector2 bottomCorner;
    private Vector2 topCorner;

    //boundary limits
    [HideInInspector]
    public float minX { get; set; }
    [HideInInspector]
    public float maxX { get; set; }
    [HideInInspector]
    public float minY { get; set; }
    [HideInInspector]
    public float maxY { get; set; }
    [HideInInspector]
    public SpriteRenderer sprite { get; set; }

    private Vector3 orientation = new Vector3(0f, 1f, 0f);

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();

        GetScreenBoundary();

        PopulateBoundaryLimits();

        gameObject.transform.Rotate(new Vector3(0,0, Random.Range(0f, 360f)));        
    }

    void Update()
    {       
        float _stepFwd = Time.deltaTime * 2f;

        gameObject.transform.Translate(orientation * _stepFwd);

        CheckOutsideLimits();
    }

    #region Boundary Stuffs
    /// <summary>
    /// Verifica os limites da tela e aplica a nova posição caso o gameObject sai de visao
    /// </summary>
    public virtual void CheckOutsideLimits()
    {
        Vector3 newPosition = gameObject.transform.position;

        if (gameObject.transform.position.x < minX)
        {
            newPosition.x = maxX;
        }

        if (gameObject.transform.position.x > maxX)
        {
            newPosition.x = minX;
        }

        if (gameObject.transform.position.y < minY)
        {
            newPosition.y = maxY;
        }

        if (gameObject.transform.position.y > maxY)
        {
            newPosition.y = minY;
        }

        if (newPosition != gameObject.transform.position)
        {
            gameObject.transform.position = newPosition;
        }

    }

    /// <summary>
    /// Popula os limites de tela
    /// </summary>
    private void PopulateBoundaryLimits()
    {
        minX = bottomCorner.x - sprite.bounds.extents.x;
        maxX = topCorner.x + sprite.bounds.extents.x;

        minY = bottomCorner.y - sprite.bounds.extents.y;
        maxY = topCorner.y + sprite.bounds.extents.y;
    }

    void GetScreenBoundary()
    {
        cameraDistance = Vector3.Distance(transform.position, Camera.main.transform.position);

        bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, cameraDistance));
        topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, cameraDistance));
    }
    #endregion
}
