﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXmanager : MonoBehaviour {

    public ObjectPoolManager poolFX;

    public GameManager gamemanager;

    public Queue<GameObject> FXRedQueue;

    void Start()
    {
        gamemanager.OnDestroyAsteroid += HitBulltetAsteroid;
        gamemanager.OnDestroyEnemy += HitBulletShip;
        gamemanager.OnPlayerHitted += HitAsteroidShip;

        FXRedQueue = new Queue<GameObject>();

    }
    /// <summary>
    /// Instancia FX ao destruir asteroid
    /// </summary>
    /// <param name="_asteroid"></param>
    /// <param name="_point"></param>
    private void HitBulltetAsteroid(GameObject _asteroid, Vector2 _point)
    {
        Transform _go = (Transform)poolFX.Instantiate(PrefabsEnum.FXRed, _point, typeof(Transform));

       FXRedQueue.Enqueue(_go.gameObject);
       Invoke("RecycleFXRed", 2f);
    }

    /// <summary>
    /// Instancia FX ao destruir nave inimiga
    /// </summary>
    /// <param name="_asteroid"></param>
    /// <param name="_point"></param>
    private void HitBulletShip(GameObject _asteroid, Vector2 _point)
    {
        Transform _go = (Transform)poolFX.Instantiate(PrefabsEnum.FXRed, _point, typeof(Transform));

        FXRedQueue.Enqueue(_go.gameObject);
        Invoke("RecycleFXRed", 2f);
    }

    /// <summary>
    /// Instancia FX ao sofrer danos
    /// </summary>
    /// <param name="_point"></param>
    private void HitAsteroidShip(Vector2 _point)
    {
        Transform _go = (Transform)poolFX.Instantiate(PrefabsEnum.FXRed, _point, typeof(Transform));

        FXRedQueue.Enqueue(_go.gameObject);
        Invoke("RecycleFXRed", 2f);
    }

    /// <summary>
    /// Recicla por tempo FX 
    /// </summary>
    private void RecycleFXRed()
    {
        poolFX.RecicleObject(PrefabsEnum.FXRed, FXRedQueue.Dequeue());
    }

    void OnDestroy()
    {
        gamemanager.OnDestroyAsteroid -= HitBulltetAsteroid;
        gamemanager.OnDestroyEnemy -= HitBulletShip;
        gamemanager.OnPlayerHitted -= HitAsteroidShip;
    }
}
