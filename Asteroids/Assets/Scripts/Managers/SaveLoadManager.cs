﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading;

public class SaveLoadManager: ScriptableObject
{
    private readonly string s_anonymousKey = "anonymous";
    public string s_scoreKey = "Score";
    public int limit = 10;

    string dataPath = "/data.dat";      

    volatile Dictionary<string, LeaderboardComponent> loadedData;

    /// <summary>
    /// Inicializa leaderboard caso nao exista
    /// </summary>
    public void Initialize()
    {
        if (Load() == null)
        {
            Dictionary<string, LeaderboardComponent> _leaderboard = new Dictionary<string,LeaderboardComponent>();

            for (int i = 0; i < limit; i++)
            {
                LeaderboardComponent _score = new LeaderboardComponent(s_anonymousKey, (10 - i));
                _leaderboard.Add(s_scoreKey + i, _score);
            }

            Save(_leaderboard);
        }
    }

    /// <summary>
    /// Salva leaderboard no pc
    /// </summary>
    /// <param name="_data"></param>
    public void Save(Dictionary<string, LeaderboardComponent> _data)
    {
        var path = Application.persistentDataPath + dataPath;
        var thread = new Thread(() => 
        {
            BinaryFormatter bf = new BinaryFormatter();
            while (true)
            {
                try
                {
                    using (FileStream file = File.Create(path))
                    {
                        bf.Serialize(file, _data);
                        file.Close();
                    }
                }
                catch (Exception)
                {
                    Debug.Log("Data Save failed, retrying...");
                    Thread.Sleep(500);
                }
                //Debug.Log("Data Saved");
                break;
            }
        });
        thread.Start();
        thread.Join();
    }

    /// <summary>
    /// Carrega leaderboard caso exista
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, LeaderboardComponent> Load()
    {
        var path = Application.persistentDataPath + dataPath;
        var thread = new Thread(() =>
        {
            while (true)
            {
                try
                {
                    if (File.Exists(path))
                    {
                        BinaryFormatter bf = new BinaryFormatter();
                        using (FileStream file = File.Open(path, FileMode.Open))
                        {
                            loadedData = (Dictionary<string, LeaderboardComponent>)bf.Deserialize(file);
                            file.Close();
                        }
                    }
                    else
                    {
                        loadedData = null;
                    }
                    //Debug.Log("Data Loaded");
                    break;
                }
                catch (Exception)
                {
                    Debug.Log("Data load failed, retrying...");
                    Thread.Sleep(500);
                }
            }
        });
        thread.Start();
        thread.Join();
        return loadedData;

    }

    /// <summary>
    /// Deleta leaderboard
    /// </summary>
    public void Delete()
    {
        var path = Application.persistentDataPath + dataPath;
        var thread = new Thread(() =>
        {
            while (true)
            {
                try
                {
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    //Debug.Log("Data deleted");
                    break;
                }
                catch (Exception)
                {
                    Debug.Log("Data delete failed, retrying...");
                    Thread.Sleep(500);
                }
            }
        });
        thread.Start();
        thread.Join();
    }

}
