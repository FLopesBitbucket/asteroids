﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlowControl : Singleton<FlowControl>
{
    protected FlowControl() { }

    private GameModes gamemode;
    private GamePresets gamepressets = GamePresets.red;

    private readonly string s_mainmenuName = "Menu";
    private readonly string s_gameplayName = "Gameplay";

    private SaveLoadManager data;

    private int lastScore = 0;
    
    #region Leaderboard Stufs

    /// <summary>
    /// Inicializa leaderboard
    /// </summary>
    void InitializeData()
    {
        data = ScriptableObject.CreateInstance<SaveLoadManager>();
        data.Initialize();
    }

    /// <summary>
    /// incrementa score
    /// </summary>
    /// <param name="_score"></param>
    public void UpdateScore(int _score)
    {
        InitializeData();

        LeaderboardComponent _result = new LeaderboardComponent(System.Environment.MachineName, _score);

        Dictionary<string, LeaderboardComponent> _leaderboard = data.Load();

        for (int i = 0; i < data.limit; i++)
        {
            if (_result.score > _leaderboard[data.s_scoreKey + i].score)
            {
                for (int j = data.limit - 1; j > i; j--)
                {
                    _leaderboard[data.s_scoreKey + j] = _leaderboard[data.s_scoreKey + (j - 1)];
                }

                _leaderboard[data.s_scoreKey + i] = _result;
                break;
            }
        }

        SetLastScore(_score);

        data.Save(_leaderboard);
    }

    /// <summary>
    /// Retorna leaderboarddata
    /// </summary>
    /// <returns></returns>
    public SaveLoadManager GetData()
    {
        if (data == null)
        {
            InitializeData();
        }

        return data;
    }

    /// <summary>
    /// Referencia último score ganho, usado para FX no menu
    /// </summary>
    /// <param name="_last"></param>
    public void SetLastScore(int _last)
    {
        lastScore = _last;
    }

    /// <summary>
    /// Retorna último score ganho
    /// </summary>
    /// <returns></returns>
    public int GetLastScore()
    {
        return lastScore;
    }

    #endregion

    /// <summary>
    /// Carrega cena menu
    /// </summary>
    public void LoadMenu()
    {
        SceneManager.LoadScene(s_mainmenuName);
    }

    /// <summary>
    /// Carrega cena gameplay
    /// </summary>
    public void LoadGameplay()
    {
        SceneManager.LoadScene(s_gameplayName);
    }

    #region Game Methods

    /// <summary>
    /// Seta GameMode
    /// </summary>
    /// <param name="_gamemode"></param>
    public void SetGameMode (GameModes _gamemode)
    {
        gamemode = _gamemode;
    }
    /// <summary>
    /// Retorna GameMode
    /// </summary>
    /// <returns></returns>
    public GameModes GetGameMode()
    {
        return gamemode;
    }

    /// <summary>
    /// Seta Game presset
    /// </summary>
    /// <param name="_pressets"></param>
    public void SetGamePressets(GamePresets _pressets)
    {
        gamepressets = _pressets;
    }

    /// <summary>
    /// Retorna GamePresset
    /// </summary>
    /// <returns></returns>
    public GamePresets GetGamePressets()
    {
        return gamepressets;
    }  

    #endregion


}
