﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIGameManager : MonoBehaviour {

    public GameManager gamemanager;
    private PlayerShip player;
    public EventSystem currentEventSystem;
    public CustomInput playerInput;
    private GameObject lastselect;

    [Header("HUD")]
    public Text liveLabel;
    public Text scoreLabel;
    public Image shipImage;

    [Header("Start")]
    public GameObject pressStart;

    [Header("Pause")]
    public Text titleLabel;
    public GameObject pauseContainer;
    public GameObject pauseSubGroup;
    public GameObject exitPopup;
    public GameObject pauseNavegationDefaultBtn;
    public GameObject popupExitNavegationDefaultBtn;

    [Header("End")]
    public GameObject endgameHideBtn;

    private readonly string s_pauseTitle = "Paused";
    private readonly string s_endTitle = "GAME OVER";

    #region Fires and Delegates

    public delegate void NavegationEvents();
    public event NavegationEvents OnClick;
    public event NavegationEvents OnNavegation;
    public event NavegationEvents OnBack;

    private void FireOnClick()
    {
        if (OnClick != null)
        {
            OnClick();
        }
    }

    public void FireOnNavegation()
    {
        if (OnNavegation != null)
        {
            OnNavegation();
        }
    }

    private void FireOnBack()
    {
        if (OnBack != null)
        {
            OnBack();
        }
    }

    #endregion     

    void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerShip>();
    }

	void Start () 
    {
        gamemanager.OnGameStart += gamemanager_OnGameStart;
        gamemanager.OnScoreUpdated += UpdateScore;
        gamemanager.OnGamePaused += OnPause;
        gamemanager.OnGameResume += OnResume;
        gamemanager.OnGameEnds += OnEndgame;

        player.OnUpdateLife += UpdateLive;
        player.OnShipSetted += ShipUI;
	}

    /// <summary>
    /// Inicializa e configura sprite da nave na UI
    /// </summary>
    /// <param name="_type"></param>
    private void ShipUI(int _type)
    {
        shipImage.sprite = gamemanager.colorAtlas[4].GetSprite(GetUISpriteShip(_type));
    }

    /// <summary>
    /// Retorna string com nome da sprite
    /// </summary>
    /// <param name="_index"></param>
    /// <returns></returns>
    private string GetUISpriteShip(int _index)
    {
        string _name = "";

        switch (FlowControl.Instance.GetGamePressets())
        {
            case GamePresets.blue:
                if (_index == 0)
                {
                    _name = "playerLife1_blue";
                }
                else if (_index == 1)
                {
                    _name = "playerLife2_blue";
                }
                else if (_index == 2)
                {
                    _name = "playerLife3_blue";
                }
                break;
            case GamePresets.red:
                if (_index == 0)
                {
                    _name = "playerLife1_red";
                }
                else if (_index == 1)
                {
                    _name = "playerLife2_red";
                }
                else if (_index == 2)
                {
                    _name = "playerLife3_red";
                }
                break;
            case GamePresets.green:
                if (_index == 0)
                {
                    _name = "playerLife1_green";
                }
                else if (_index == 1)
                {
                    _name = "playerLife2_green";
                }
                else if (_index == 2)
                {
                    _name = "playerLife3_green";
                }
                break;
            case GamePresets.orange:
                if (_index == 0)
                {
                    _name = "playerLife1_orange";
                }
                else if (_index == 1)
                {
                    _name = "playerLife2_orange";
                }
                else if (_index == 2)
                {
                    _name = "playerLife3_orange";
                }
                break;
            default:
                break;
        }

        return _name;
    }

    #region EndGame

    /// <summary>
    /// callback fim de jogo
    /// </summary>
    private void OnEndgame()
    {
        pauseContainer.SetActive(true);
        endgameHideBtn.SetActive(false);

        titleLabel.text = s_endTitle;

        Invoke("DelayInputsUI", 1.5f);
    }

    /// <summary>
    /// Delay manual para impedir clicks nao intencionais ao perder
    /// </summary>
    private void DelayInputsUI()
    {
        currentEventSystem.SetSelectedGameObject(pauseNavegationDefaultBtn);
        lastselect = pauseNavegationDefaultBtn;
    }

    #endregion

    #region Pause
    /// <summary>
    /// Callback pausar jogo
    /// </summary>
    private void OnPause()
    {
        pauseContainer.SetActive(true);
        pauseSubGroup.SetActive(true);
        titleLabel.text = s_pauseTitle;

        currentEventSystem.SetSelectedGameObject(pauseNavegationDefaultBtn);
        lastselect = pauseNavegationDefaultBtn;
    }

    /// <summary>
    /// Callback despausar jogo
    /// </summary>
    private void OnResume()
    {
        pauseContainer.SetActive(false);
    }

    /// <summary>
    /// Abrir menu tem certeza de sair
    /// </summary>
    public void GoToMenuPopup()
    {
        pauseSubGroup.SetActive(false);
        exitPopup.SetActive(true);
        currentEventSystem.SetSelectedGameObject(popupExitNavegationDefaultBtn);
        lastselect = popupExitNavegationDefaultBtn;

        FireOnClick();
    }

    /// <summary>
    /// Ir ao menu
    /// </summary>
    public void GoToMenuYes()
    {
        FireOnClick();

        FlowControl.Instance.LoadMenu();
    }

    /// <summary>
    /// Fechar popup tem certeza de sair
    /// </summary>
    public void GoToMenuNo()
    {
        exitPopup.SetActive(false);
        pauseSubGroup.SetActive(true);
        currentEventSystem.SetSelectedGameObject(pauseNavegationDefaultBtn);
        lastselect = pauseNavegationDefaultBtn;

        FireOnBack();
    }

    /// <summary>
    /// Reiniciar jogo
    /// </summary>
    public void Retry()
    {
        FireOnClick();

        FlowControl.Instance.LoadGameplay();
    }

    /// <summary>
    /// despausar jogo
    /// </summary>
    public void Resume()
    {
        FireOnClick();

        gamemanager.SetPausedStatus(false);
    }

    #endregion

    #region HUD

    /// <summary>
    /// Atualiza score UI
    /// </summary>
    /// <param name="scoreCount"></param>
    void UpdateScore(int scoreCount)
    {
        scoreLabel.text = scoreCount.ToString("0000000");
    }

    /// <summary>
    /// Atualzia vidas UI
    /// </summary>
    /// <param name="liveCount"></param>
    void UpdateLive(int liveCount)
    {
        liveLabel.text = liveCount.ToString("00");
    }

    #endregion
    /// <summary>
    /// Callback game start
    /// </summary>
    void gamemanager_OnGameStart()
    {
        pressStart.SetActive(false);
    }

    void Update()
    {
        if (currentEventSystem.currentSelectedGameObject == null)
        {
            currentEventSystem.SetSelectedGameObject(lastselect);
        }
        else
        {
            lastselect = currentEventSystem.currentSelectedGameObject;
        }
    }

    void OnDestroy()
    {
        gamemanager.OnGameStart -= gamemanager_OnGameStart;
        gamemanager.OnScoreUpdated -= UpdateScore;
        gamemanager.OnGamePaused -= OnPause;
        gamemanager.OnGameResume -= OnResume;
        gamemanager.OnGameEnds -= OnEndgame;

        player.OnUpdateLife -= UpdateScore;
        player.OnShipSetted -= ShipUI;
    }
	
}
