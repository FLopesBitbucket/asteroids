public enum PrefabsEnum
{
    //STARTENUM//	
////powerups
	PowerupShield = 300, 
//// fxs
	FXRed = 200, 
////Sounds
	AudioAsteroidDamaged = 100, 
	AudioShipHyperspace = 101, 
	AudioShipShoot = 102, 
	AudioEnemyShoot = 103, 
	AudioFlowLose = 104, 
	AudioShipDamaged = 105, 
	AudioEnemyDamaged = 106, 
	AudioFlowNewLevel = 107, 
	AudioFlowStart = 108, 
	UIClick = 109,
    AudioShieldDown = 110,
    AudioShieldUp = 111, 
////gameobjects
	OvniEnemy = 1, 
	BigAsteroid = 2, 
	MediumAsteroid = 3, 
	SmallAsteroid = 4, 
	bullet = 5, 
  
    //ENDENUM
}