using UnityEngine;
using System.Collections;

[System.Serializable]
public class PoolObject
{
	public GameObject prefab;
    public string poolObjectName;
	public int poolStep;
	public int poolStartSize;
}

class PoolItem
{
	public GameObject[] activeObjects;
	public GameObject[] inactiveObjects;

	public PoolItem(int pSize)
	{
		activeObjects = new GameObject[0];
		inactiveObjects = new GameObject[pSize];
	}
}