using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolManager : MonoBehaviour
{
	[HideInInspector]
	public PoolObject[] objectList;

    public bool destroyOnLoad;

	private Dictionary<int, PoolItem> pool;

	void Awake()
	{
		this.enabled = false;

        if (!destroyOnLoad)
        {
            DontDestroyOnLoad(this);
        }

		this.CreatePool();
	}

    /// <summary>
    /// Criar pool
    /// </summary>
	public void CreatePool()
	{
		pool = new Dictionary<int, PoolItem>();

		PoolItem _bufferItem;
		GameObject[] _itemList;
		PoolObject _bufferedObject;
		int i;
		int j;

		for (i = 0; i < objectList.Length; i++)
		{
			_bufferedObject = objectList[i];

			_bufferItem = new PoolItem(_bufferedObject.poolStartSize);
			_itemList = _bufferItem.inactiveObjects;

			for (j = 0; j < _itemList.Length; j++)
			{
				_itemList[j] = GameObject.Instantiate(_bufferedObject.prefab, Vector3.zero, Quaternion.identity) as GameObject;
				_itemList[j].SetActive(false);
				_itemList[j].transform.parent = this.transform;
			}

			PrefabsEnum pEnum = (PrefabsEnum)System.Enum.Parse(typeof(PrefabsEnum),_bufferedObject.poolObjectName);
			pool.Add(pEnum.GetHashCode(), _bufferItem);
		}
	}

    /// <summary>
    /// Instanciar objeto
    /// </summary>
    /// <param name="_prefab"></param>
    /// <param name="_type"></param>
    /// <returns></returns>
	public Object Instantiate(PrefabsEnum _prefab, System.Type _type)
	{
		return Instantiate(_prefab, Vector3.zero, Quaternion.identity, _type);
	}

    /// <summary>
    /// instanciar objeto
    /// </summary>
    /// <param name="_prefab"></param>
    /// <param name="_position"></param>
    /// <param name="_type"></param>
    /// <returns></returns>
    public Object Instantiate(PrefabsEnum _prefab, Vector2 _position, System.Type _type)
    {
        return Instantiate(_prefab, new Vector3(_position.x, _position.y), Quaternion.identity, _type);
    }

    /// <summary>
    /// instanciar objeto
    /// </summary>
    /// <param name="_prefab"></param>
    /// <param name="_position"></param>
    /// <param name="_rotation"></param>
    /// <param name="_type"></param>
    /// <returns></returns>
	public Object Instantiate(PrefabsEnum _prefab, Vector3 _position, Quaternion _rotation, System.Type _type)
	{
		PoolItem _bufferItem;
		pool.TryGetValue(_prefab.GetHashCode(), out _bufferItem);

		if (_bufferItem == null)
		{
			Debug.LogWarning("Instantiate :: No Object Found!!! :: " + _prefab.ToString());
			return null;
		}

		GameObject _item;
		int i;

		GameObject[] _newItemList;
		int _length;

		if (_bufferItem.inactiveObjects.Length == 0)
		{
			ExpandPool(_bufferItem, _prefab);
            print("~EXPAND POOL~");
		}

		_item = _bufferItem.inactiveObjects[0];

		// remove from inactive list
		_newItemList = new GameObject[_bufferItem.inactiveObjects.Length - 1];
		_length = _newItemList.Length;
		for (i = 0; i < _length; i++)
		{
			_newItemList[i] = _bufferItem.inactiveObjects[i + 1];
		}

		_bufferItem.inactiveObjects = _newItemList;


		// add to active list
		_newItemList = new GameObject[_bufferItem.activeObjects.Length + 1];
		_length = _bufferItem.activeObjects.Length;

		for (i = 0; i < _length; i++)
		{
			_newItemList[i] = _bufferItem.activeObjects[i];
		}
		_newItemList[_newItemList.Length - 1] = _item;

		_bufferItem.activeObjects = _newItemList;


		_item.transform.parent = null;
		_item.SetActive(true);
		_item.transform.position = _position;
		_item.transform.rotation = _rotation;

		return _item.GetComponent(_type);

	}

    /// <summary>
    /// reciclar objeto
    /// </summary>
    /// <param name="_prefab"></param>
    /// <param name="_instance"></param>
	public void RecicleObject(PrefabsEnum _prefab, GameObject _instance)
	{
		PoolItem _bufferItem;
		pool.TryGetValue(_prefab.GetHashCode(), out _bufferItem);
		int i;
		int j;
		int k;
		GameObject _bufferObject;
		GameObject[] _newList;
		int _activeLength = _bufferItem.activeObjects.Length;
		int _inactiveLength = _bufferItem.inactiveObjects.Length;

		for (i = 0; i < _activeLength; i++)
		{
			_bufferObject = _bufferItem.activeObjects[i];
			if (_bufferObject && _bufferObject == _instance)
			{
				_bufferObject.transform.parent = this.transform;
				_bufferObject.SetActive(false);

				k = 0;
				// remove from active list
				_newList = new GameObject[_bufferItem.activeObjects.Length - 1];
				for (j = 0; j < _activeLength; j++)
				{
					if (i != j)
					{
						_newList[k] = _bufferItem.activeObjects[j];
						k++;
					}
				}

				_bufferItem.activeObjects = _newList;

				// add to inactive list

				_newList = new GameObject[_bufferItem.inactiveObjects.Length + 1];
				_newList[_bufferItem.inactiveObjects.Length] = _instance;

				for (j = 0; j < _inactiveLength; j++)
				{
					_newList[j] = _bufferItem.inactiveObjects[j];
				}

				_bufferItem.inactiveObjects = _newList;

				return;
			}
		}

		Debug.LogWarning("RecicleObject :: No Object Found!!! :: " + _prefab.ToString() + " :: " + _instance);
	}


    /// <summary>
    /// expandir pool se necessário
    /// </summary>
    /// <param name="_poolItem"></param>
    /// <param name="_prefab"></param>
	private void ExpandPool(PoolItem _poolItem, PrefabsEnum _prefab)
	{
		GameObject[] _newItemList;
		PoolObject _bufferObject = null;
		int i;
		bool _foundItem = false;
		int _length = objectList.Length;
		int _newListLength;
     
		for (i = 0; i < _length; i++)
		{
			_bufferObject = objectList[i];                     

			if (_bufferObject.poolObjectName == _prefab.ToString())
			{
				_foundItem = true;
				break;
			}
		}

		if (_foundItem)
		{
			_newItemList = new GameObject[_bufferObject.poolStep];
			_newListLength = _newItemList.Length;

			for (i = 0; i < _newListLength; i++)
			{
				_newItemList[i] = GameObject.Instantiate(_bufferObject.prefab, Vector3.zero, Quaternion.identity) as GameObject;
				_newItemList[i].SetActive(false);
			}

			_poolItem.inactiveObjects = _newItemList;
            print("Expanding pool... Inactive Objects: " + _poolItem.inactiveObjects.Length);
		}
		else
		{
			Debug.LogWarning("ExpandPool :: Nao achou o item no pool!!!!");
		}
	}
}