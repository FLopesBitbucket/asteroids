﻿public enum ObjectType { shipPlayer, shipEnemy, bulletPlayer, bulletEnemy, asteroid, shield}
public enum ShipType { playerShip, enemyShip }
public enum AsteroidsType { Big, Medium, Small}
public enum GameModes { classic, advanced}
public enum GameState { pregame, playing, paused, ended}
public enum PoolType { gameplay, UI}
public enum GamePresets { blue = 0, red =1, green=2, orange=3 }