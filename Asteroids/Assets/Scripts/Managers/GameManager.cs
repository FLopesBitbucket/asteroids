﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class GameManager : MonoBehaviour
{
    //[Header("Delegates")]
    public delegate void GameFlow();
    public event GameFlow OnGameStart;
    public event GameFlow OnGamePaused;
    public event GameFlow OnGameResume;
    public event GameFlow OnGameEnds;

    public delegate void GeneralCollision(GameObject _asteroid, Vector2 _point);
    public event GeneralCollision OnDestroyAsteroid;
    public event GeneralCollision OnDestroyEnemy;

    public delegate void CollisionPlayer(Vector2 _point);
    public event CollisionPlayer OnPlayerHitted;

    public delegate void ScoreUpdate(int _score);
    public event ScoreUpdate OnScoreUpdated;

    public delegate void NewLevel();
    public event NewLevel OnNewLevel;

    public delegate void SpawnEnemy();
    public event SpawnEnemy OnSpawnEnemy;

    public delegate void ShieldPowerupActions();
    public event ShieldPowerupActions OnPickShield;
    public event ShieldPowerupActions OnBreakShield;

    [Header("General References")]
    public CustomInput playerInput;
    //private bool initialized;
    [SerializeField]
    private ObjectPoolManager bulletPool;

    [Header("Atlas")]
    public SpriteAtlas[] colorAtlas;

    [Header("Game References")]
    private GameState gamestate = GameState.pregame;
    private int gameLevel = 1;
    private int totalScore = 0000;
    private int asteroidsActives = 0;
    private int enemysActives = 0;
    private bool enemySpawned = false;
    private bool gamePaused = false;
    private bool shielded = false;

    [Header("Screen Boundary Variables")]
    private float cameraDistance;
    private Vector2 bottomCorner;
    private Vector2 topCorner;

    void Awake()
    {
        GetScreenBoundary();
    }

    void Update()
    {
        if (gamestate == GameState.playing)
        {
            if (playerInput.GetActionStart())
            {
                SetPausedStatus(!gamePaused);
            }
        }
        else if (gamestate == GameState.pregame)
        {
            if (playerInput.GetActionStart())
            {
                FireOnGameStart();
                FireOnNewLevel();
                gamestate = GameState.playing;
            }
        }
       
    }

    /// <summary>
    /// Inicializa callbacks de fim de jogo, salva score
    /// </summary>
    public void Die()
    {
        gamestate = GameState.ended;
        FireOnGameEnds();

        FlowControl.Instance.UpdateScore(totalScore);

    }

    #region Collision Controller

    /// <summary>
    /// Controla colisoes
    /// </summary>
    /// <param name="_type1"></param>
    /// <param name="_type2"></param>
    /// <param name="_point"></param>
    /// <param name="_obj1"></param>
    /// <param name="_obj2"></param>
    public void Collision(ObjectType _type1, ObjectType _type2, Vector2 _point, GameObject _obj1, GameObject _obj2)
    {
        if (_type1 == ObjectType.bulletPlayer)
        {
            switch (_type2)
            {
                case ObjectType.shipPlayer: 
                    break;
                case ObjectType.shipEnemy:
                    KillEnemy(_obj2, _point);
                    break;
                case ObjectType.bulletPlayer:
                    break;
                case ObjectType.bulletEnemy:
                    break;
                case ObjectType.asteroid:
                    KillAsteroid(_obj2, _point);
                    break;
                default:
                    break;
            }
        }
        else if (_type1 == ObjectType.asteroid || _type1 == ObjectType.shipEnemy || _type1 == ObjectType.bulletEnemy)
        {
            if (_type2 == ObjectType.shipPlayer)
            {
                Hitted(_point);
            }
        }
        else if (_type1 == ObjectType.shield)
        {
            if (_type2 == ObjectType.shipPlayer)
            {
                ShieldPowerup();
            }
        }
    }

    /// <summary>
    /// Ativa callback shieldup
    /// </summary>
    void ShieldPowerup()
    {
        shielded = true;
        FireOnPickShield();
    }

    /// <summary>
    /// Ativa callback player hitted e shield break
    /// </summary>
    /// <param name="_point"></param>
    void Hitted(Vector2 _point)
    {
        if (shielded)
            FireOnBreakShield(); shielded = false;

        FireOnPlayerHitted(_point);
    }

    /// <summary>
    /// Ativa callback inimigo morto
    /// </summary>
    /// <param name="_enemy"></param>
    /// <param name="_point"></param>
    void KillEnemy(GameObject _enemy, Vector2 _point)
    {
        FireOnDestroyEnemy(_enemy, _point);
    }

    /// <summary>
    /// Ativa callback Asteroid destruido
    /// </summary>
    /// <param name="_asteroid"></param>
    /// <param name="_point"></param>
    void KillAsteroid(GameObject _asteroid, Vector2 _point)
    {
        FireOnDestroyAsteroid(_asteroid, _point);
    }

    #endregion

    #region Boundarys
    /// <summary>
    /// Configura limites de tela geral
    /// </summary>
    void GetScreenBoundary()
    {
        cameraDistance = Vector3.Distance(transform.position, Camera.main.transform.position);

        bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, cameraDistance));
        topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, cameraDistance));
    }
    #endregion

    #region public methods
    /// <summary>
    /// Verifica inimigos ativos
    /// </summary>
    /// <param name="_value"></param>
    public void CheckActiveEnemys(int _value)
    {
        enemysActives = _value;

        CheckClearStage();
    }

    /// <summary>
    /// Verifica asteroids ativos
    /// </summary>
    /// <param name="_value"></param>
    public void CheckActiveAsteroids(int _value)
    {
        asteroidsActives = _value;

        if (_value == 1 && enemySpawned == false)
        {
            enemySpawned = true;
            FireOnSpawnEnemy();
        }

        CheckClearStage();
    }

    /// <summary>
    /// Verifica se existem inimigos e/ou asteroids ativos
    /// </summary>
    void CheckClearStage()
    {
        int _total = asteroidsActives + enemysActives;

        if (_total <= 0)
        {
            UpdateLevel();
            FireOnNewLevel();
            enemySpawned = false;
        }
    }

    /// <summary>
    /// Ativa callback update score
    /// </summary>
    /// <param name="_score"></param>
    public void UpdateScore(int _score)
    {
        totalScore += _score;
        FireOnScoreUpdated(totalScore);
    }

    /// <summary>
    /// Incrementa nivel
    /// </summary>
    public void UpdateLevel()
    {
        gameLevel++;
    }

    /// <summary>
    /// retorna atual nivel
    /// </summary>
    /// <returns></returns>
    public int GetActualLevel()
    {
        return gameLevel;
    }

    /// <summary>
    /// Retorna limite canto inferior
    /// </summary>
    /// <returns></returns>
    public Vector2 GetBottomCorner()
    {
        return bottomCorner;
    }

    /// <summary>
    /// Retorna limite canto superior
    /// </summary>
    /// <returns></returns>
    public Vector2 GetTopCorner()
    {
        return topCorner;
    }

    /// <summary>
    /// Retorna pool de projeteis
    /// </summary>
    /// <returns></returns>
    public ObjectPoolManager GetBulletPool()
    {
        return bulletPool;
    }

    /// <summary>
    /// Retorna gamestate
    /// </summary>
    /// <returns></returns>
    public GameState GetGameState()
    {
        return gamestate;
    }

    /// <summary>
    /// Retorna game status
    /// </summary>
    /// <returns></returns>
    public bool GetPausedStatus()
    {
        return gamePaused;
    }

    /// <summary>
    /// Seta gamestatus
    /// </summary>
    /// <returns></returns>
    public void SetPausedStatus(bool _isPaused)
    {
        gamePaused = _isPaused;

        if (gamePaused)
        {
            gamestate = GameState.paused;
            FireOnGamePaused();
        }
        else
        {
            gamestate = GameState.playing;
            FireOnGameResume();
        }
    }

    #endregion

    #region Fires

    private void FireOnGameStart()
    {
        if (OnGameStart != null)
        {
            OnGameStart();
        }
    }

    private void FireOnGamePaused()
    {
        if (OnGamePaused != null)
        {
            OnGamePaused();
        }
    }

    private void FireOnGameResume()
    {
        if (OnGameResume != null)
        {
            OnGameResume();
        }
    }

    private void FireOnGameEnds()
    {
        if (OnGameEnds != null)
        {
            OnGameEnds();
        }
    }

    private void FireOnDestroyAsteroid(GameObject _asteroid, Vector2 _point)
    {
        if (OnDestroyAsteroid != null)
        {
            OnDestroyAsteroid(_asteroid, _point);
        }
    }

    private void FireOnPlayerHitted(Vector2 _point)
    {
        if (OnPlayerHitted != null)
        {
            OnPlayerHitted(_point);
        }
    }

    private void FireOnScoreUpdated(int _score)
    {
        if (OnScoreUpdated != null)
        {
            OnScoreUpdated(_score);
        }
    }

    private void FireOnNewLevel()
    {
        if (OnNewLevel != null)
        {
            OnNewLevel();
        }
    }

    private void FireOnSpawnEnemy()
    {
        if (OnSpawnEnemy != null)
        {
            OnSpawnEnemy();
        }
    }

    private void FireOnDestroyEnemy(GameObject _enemy, Vector2 _point)
    {
        if (OnDestroyEnemy != null)
        {
            OnDestroyEnemy(_enemy, _point);
        }
    }

    private void FireOnPickShield()
    {
        if (OnPickShield != null)
        {
            OnPickShield();
        }
    }

    private void FireOnBreakShield()
    {
        if (OnBreakShield != null)
        {
            OnBreakShield();
        }
    }
    
    #endregion
}
