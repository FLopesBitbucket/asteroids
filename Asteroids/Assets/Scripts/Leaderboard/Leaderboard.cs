﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LeaderboardComponent
{
    public string name;
    public int score;

    public LeaderboardComponent(string _name, int _score)
    {
        name = _name;
        score = _score;
    }
}
