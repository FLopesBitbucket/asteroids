﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : Ship
{
    public delegate void UpdateLife(int _life);
    public event UpdateLife OnUpdateLife;
      
    public delegate void ShipDamaged();
    public event ShipShoot OnShipDamaged;

    public delegate void PlayerShipHyperSpace();
    public event PlayerShipHyperSpace OnShipHyperSpace;

    public delegate void SetShipType(int _value);
    public event SetShipType OnShipSetted;

    public CustomInput playerInput;

    [Header("Ship Parameters")]
    public Animator shipanim; 
    public int life = 3;
    public int hyperspacePowerup = 3;
    public GameObject shieldObj;
    public bool inShield = false;

    public float invelnerableTime = 1f;
    private bool invulnerable = false;

    private int shipTypeNum = 0;

    [Header("Inertia Parameters")]
    public float maxInertia = 1f;
    public float shipInInertiaSpeed = 0.1f;
    public float increaseInertia = 2f;
    public float descreaseInertia = 1f;

    private float acumulateInertia = 0f;

    private int a_invulnerable = Animator.StringToHash("invulnerable");
        
    public override void Start() 
    {
        base.Start();

        gamemanager.OnPlayerHitted += gamemanager_OnPlayerHitted;
        gamemanager.OnPickShield += Shield;

        FireOnUpdateLife(life);

        InitializeColors();

	}

    void Update () 
    {
        if (!initialized || gamemanager.GetGameState() != GameState.playing)
            return;

        CheckInvulnerable();

        if (playerInput.GetVerticalSpeed() <= 0f)
        {
            Inertia();
        }
        else 
        {
            Move(playerInput.GetHorizontalSpeed(), playerInput.GetVerticalSpeed(), shipSpeedForward, shipSpeedRotate);

            if (acumulateInertia <= maxInertia)
            {
                acumulateInertia += Time.deltaTime * increaseInertia;
            }
            else
            {
                acumulateInertia = maxInertia;
            }
        }

        if (playerInput.GetActionA())
        {
            NormalShoot();
        }

        if (playerInput.GetActionB())
        {
            HyperSpace(false);
        }

        CheckOutsideLimits();

	}

    /// <summary>
    /// Ativa shield
    /// </summary>
    private void Shield()
    {
        inShield = true;
        shieldObj.SetActive(true);
    }

    /// <summary>
    /// Quebra shield 
    /// </summary>
    private void BreakShield()
    {
        inShield = false;
        shieldObj.SetActive(false);
    }
     

    #region Colors
    
    /// <summary>
    /// inicializa e muda cor e sprite por tipo
    /// </summary>
    void InitializeColors()
    {
        sprite.sprite = gamemanager.colorAtlas[(int)FlowControl.Instance.GetGamePressets()].GetSprite(RandomizeSpriteShip());
        FireOnShipSetted(shipTypeNum);
    }

    /// <summary>
    /// Randomiza tipo da nave
    /// </summary>
    /// <returns></returns>
    private string RandomizeSpriteShip()
    {
        int _random = Random.Range(0, 3);
        shipTypeNum = _random;

        string _shipname = "";

        if (_random == 0)
        {
            _shipname = s_ship1;
        }
        else if (_random == 1)
        {
            _shipname = s_ship2;
        }
        else if (_random == 2)
        {
            _shipname = s_ship3;
        }

        return _shipname;
    }

    #endregion

    #region Respawn Stuffs

    /// <summary>
    /// CAllback sofrer danos
    /// </summary>
    /// <param name="_point"></param>
    void gamemanager_OnPlayerHitted(Vector2 _point)
    {
        if (inShield)
        {
            BreakShield();
        }
        else
        {
            if (invulnerable || life <= 0)
            {
                return;
            }

            life--;
            FireOnUpdateLife(life);
            FireOnShipDamaged();

            if (life <= 0)
            {
                gamemanager.Die();
            }
            else
            {

                Respawn();
            }
        }
       
    }

    /// <summary>
    /// Respawn após sofrer dano
    /// </summary>
    void Respawn()
    {
        shipanim.SetBool(a_invulnerable, true);
        acumulateInertia = 0f;

        invulnerable = true;

        HyperSpace(true);       
    }
 
    /// <summary>
    /// verifica invulnerabilidade da nave
    /// </summary>
    void CheckInvulnerable()
    {
        if (invulnerable)
        {
            invelnerableTime -= Time.deltaTime;

            if (invelnerableTime <= 0f)
            {
                invulnerable = false;
                invelnerableTime = 1f;
                shipanim.SetBool(a_invulnerable, false);
            }
        }
    }
    #endregion

    #region Skills

    /// <summary>
    /// Ativa teleport do hyperspace
    /// </summary>
    /// <param name="_isRespawn"></param>
    private void HyperSpace(bool _isRespawn)
    {
        float _spawnPosX = Random.Range(gamemanager.GetBottomCorner().x, gamemanager.GetTopCorner().x);
        float _spawnPosY = Random.Range(gamemanager.GetBottomCorner().y, gamemanager.GetTopCorner().y);

        transform.position = new Vector3(_spawnPosX, _spawnPosY);

        if (!_isRespawn)
            FireOnShipHyperSpace();
    }

    #endregion

    #region Movement
    /// <summary>
    /// Movimenta a nave
    /// </summary>
    /// <param name="_horizontalValue"></param>
    /// <param name="_verticalValue"></param>
    /// <param name="_forwardSpeed"></param>
    /// <param name="_rotateSpeed"></param>
    public override void Move(float _horizontalValue, float _verticalValue, float _forwardSpeed, float _rotateSpeed)
    {
        base.Move(_horizontalValue, _verticalValue, _forwardSpeed, _rotateSpeed);
    }

    /// <summary>
    /// Verifica limites da tela
    /// </summary>
    public override void CheckOutsideLimits()
    {
        base.CheckOutsideLimits();
    }

    /// <summary>
    /// aplica inércia na nave
    /// </summary>
    void Inertia()
    {
        if (acumulateInertia > 0f)
        {
            acumulateInertia -= Time.deltaTime * descreaseInertia;
        }
        else
        {
            acumulateInertia = 0f;
        }

        Move(playerInput.GetHorizontalSpeed(), acumulateInertia, shipInInertiaSpeed, shipSpeedRotate);

    }
    #endregion

    #region Shoots
    /// <summary>
    /// atirar
    /// </summary>
    public override void NormalShoot()
    {
        base.NormalShoot();
    }
    #endregion

    #region Fires

    private void FireOnUpdateLife(int _life)
    {
        if (OnUpdateLife != null)
        {
            OnUpdateLife(_life);
        }
    }

    private void FireOnShipDamaged()
    {
        if (OnShipDamaged != null)
        {
            OnShipDamaged();
        }
    }

    private void FireOnShipHyperSpace()
    {
        if (OnShipHyperSpace != null)
        {
            OnShipHyperSpace();
        }
    }

    private void FireOnShipSetted(int _value)
    {
        if (OnShipSetted != null)
        {
            OnShipSetted(_value);
        }
    }


    #endregion

    
    void OnDestroy()
    {
        gamemanager.OnPlayerHitted -= gamemanager_OnPlayerHitted;
        gamemanager.OnPickShield -= Shield;
    }
}
