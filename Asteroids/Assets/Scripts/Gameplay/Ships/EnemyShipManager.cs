﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipManager : MonoBehaviour {

    public delegate void SpawnNewEnemy(EnemyShip _actual);
    public event SpawnNewEnemy OnSpawnNewEnemy;

    [Header("General References")]
    public ObjectPoolManager enemyPool;
    public GameManager gamemanager;

    [Header("Enemy Parameters")]
    public int initEnemysPerWaves = 1;

    [Header("Score Parameters")]
    public int scoreEnemy;
    private int activeEnemys;

    private void Start()
    {
        gamemanager.OnSpawnEnemy += InstantiateEnemy;
        gamemanager.OnDestroyEnemy += DestroyEnemys;
	}

    /// <summary>
    /// Instancia e inicializa ovni
    /// </summary>
    private void InstantiateEnemy()
    {
        for (int i = 0; i < initEnemysPerWaves; i++)
        {
            float _spawnPosX = Random.Range(gamemanager.GetBottomCorner().x, gamemanager.GetTopCorner().x);
            float _spawnPosY = Random.Range(gamemanager.GetBottomCorner().y, gamemanager.GetTopCorner().y);

            EnemyShip _enemy = (EnemyShip)enemyPool.Instantiate(PrefabsEnum.OvniEnemy, new Vector3(_spawnPosX, _spawnPosY), Quaternion.identity, typeof(EnemyShip));
            _enemy.PrepareShip();
            activeEnemys++;

            FireOnSpawnNewEnemy(_enemy);
        }
        CheckActiveEnemys();
    }

    /// <summary>
    /// Destroy e recicla ovni
    /// </summary>
    /// <param name="_enemyObj"></param>
    /// <param name="_point"></param>
    private void DestroyEnemys(GameObject _enemyObj, Vector2 _point)
    {
        activeEnemys--;

        RecycleAsteroid(PrefabsEnum.OvniEnemy, _enemyObj);

        gamemanager.UpdateScore(scoreEnemy);
        CheckActiveEnemys();
    }

    /// <summary>
    /// reciclar ovni
    /// </summary>
    /// <param name="_prefab"></param>
    /// <param name="_enemy"></param>
    private void RecycleAsteroid(PrefabsEnum _prefab, GameObject _enemy)
    {
        enemyPool.RecicleObject(_prefab, _enemy);
    }

    /// <summary>
    /// verifica a quantidade de inimigos ativos
    /// </summary>
    void CheckActiveEnemys()
    {
        gamemanager.CheckActiveEnemys(activeEnemys);
    }

    public void FireOnSpawnNewEnemy(EnemyShip _ship)
    {
        if (OnSpawnNewEnemy != null)
        {
            OnSpawnNewEnemy(_ship);
        }
    }

    private void OnDestroy()
    {
        gamemanager.OnSpawnEnemy -= InstantiateEnemy;
        gamemanager.OnDestroyEnemy -= DestroyEnemys;

    }
}
