﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : Ship
{
	[Header("Ship Parameters")]
    public int life = 1;
    private float verticalSpeed;
    public float cooldownShoots = 2f;
    private float _cooldownShoots;
    public float maxSpeed = 1f;

    public GameObject cannonShip;

    private PlayerShip player;

    public override void Start()
    {
        base.Start();

        player = GameObject.FindObjectOfType<PlayerShip>();

        InitializeColors();

        if (FlowControl.Instance.GetGameMode() == GameModes.advanced)
        {
            cooldownShoots = 0.5f;
            maxSpeed = 4f;
        }
    }

    /// <summary>
    /// Inicializa cor ovni
    /// </summary>
    void InitializeColors()
    {
        sprite.sprite = gamemanager.colorAtlas[(int)FlowControl.Instance.GetGamePressets()].GetSprite(s_ufo);
    }

    /// <summary>
    /// Prepara nave inimiga
    /// </summary>
    public void PrepareShip()
    {
        orientation = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f));
        verticalSpeed = Random.Range(0f, maxSpeed);
        _cooldownShoots = cooldownShoots;

        initialized = true;

        if (bulletPool == null)
        {
            bulletPool = gamemanager.GetBulletPool();
        }
    }

    void Update()
    {
        if (!initialized || gamemanager.GetGameState() != GameState.playing)
            return;

        Vector3 direction = player.transform.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 270;
        cannonShip.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        
        _cooldownShoots -= Time.deltaTime;

        if (_cooldownShoots <= 0f)
        {
            NormalShoot(cannonShip.transform);
        }
        else
        {
            Move(0f, verticalSpeed, shipSpeedForward, 0f);
        }

        CheckOutsideLimits();
    }

    #region Movement
    /// <summary>
    /// movimenta nave
    /// </summary>
    /// <param name="_horizontalValue"></param>
    /// <param name="_verticalValue"></param>
    /// <param name="_forwardSpeed"></param>
    /// <param name="_rotateSpeed"></param>
    public override void Move(float _horizontalValue, float _verticalValue, float _forwardSpeed, float _rotateSpeed)
    {
        base.Move(_horizontalValue, _verticalValue, _forwardSpeed, _rotateSpeed);
    }

    /// <summary>
    /// verifica limites
    /// </summary>
    public override void CheckOutsideLimits()
    {
        base.CheckOutsideLimits();
    }
    #endregion

    #region Shoots
    /// <summary>
    /// atirar
    /// </summary>
    /// <param name="_ref"></param>
    public override void NormalShoot(Transform _ref)
    {
        _cooldownShoots = cooldownShoots;

        base.NormalShoot(_ref);
    }
    #endregion

    void OnDisabled()
    {
        initialized = false;
    }
}
