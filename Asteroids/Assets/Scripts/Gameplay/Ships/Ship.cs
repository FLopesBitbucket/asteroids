﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour, IBoundary
{
    public delegate void ShipShoot();
    public event ShipShoot OnShipShoot;

    [Header("General References")]
    public ObjectType objType;
    protected GameManager gamemanager;
    protected bool initialized;

    [Header("Ship Parameters")]
    public float shipSpeedForward;
    public float shipSpeedRotate;

    protected Vector3 orientation = new Vector3(0f, 1f, 0f);

    //boundary limits
    [HideInInspector]
    public float minX { get; set; }
    [HideInInspector]
    public float maxX { get; set; }
    [HideInInspector]
    public float minY { get; set; }
    [HideInInspector]
    public float maxY { get; set; }
    [HideInInspector]
    public SpriteRenderer sprite { get; set; }

    [Header("Shoot Parameters")]
    public ShipType shipType;
    public ObjectPoolManager bulletPool;
    private Vector3 bulletPos = new Vector3(0, 0);

    protected readonly string s_ship1 = "playerShip1";
    protected readonly string s_ship2 = "playerShip2";
    protected readonly string s_ship3 = "playerShip3";
    protected readonly string s_ufo = "ufo";


    void Awake()
    {
        gamemanager = GameObject.FindObjectOfType<GameManager>();
    }

    public virtual void Start()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();

        gamemanager.OnGameStart += gamemanager_OnGameStart;

        PopulateBoundaryLimits();
    }
       
    /// <summary>
    /// Inicializa variaveis
    /// </summary>
    void gamemanager_OnGameStart()
    {
        initialized = true;
    }

    #region Movement    

    /// <summary>
    /// Trata movimentação da nave
    /// </summary>
    /// <param name="_horizontalValue"></param>
    /// <param name="_verticalValue"></param>
    /// <param name="_forwardSpeed"></param>
    /// <param name="_rotateSpeed"></param>
    public virtual void Move(float _horizontalValue, float _verticalValue, float _forwardSpeed, float _rotateSpeed)
    {
        float _stepFwd = Time.deltaTime * _forwardSpeed;
        float _stepRot = -Time.deltaTime * _rotateSpeed;
      
        gameObject.transform.Rotate(0f, 0f, _horizontalValue * _stepRot);

        if (_verticalValue > 0)
        {           
            gameObject.transform.Translate(orientation * _verticalValue * _stepFwd);
        }
        
    }

    /// <summary>
    /// Verifica os limites da tela e aplica a nova posição caso o gameObject sai de visao
    /// </summary>
    public virtual void CheckOutsideLimits()
    {
        Vector3 newPosition = gameObject.transform.position;

        if (gameObject.transform.position.x < minX)
        {
            newPosition.x = maxX;
        }

        if (gameObject.transform.position.x > maxX)
        {
            newPosition.x = minX;
        }

        if (gameObject.transform.position.y < minY)
        {
            newPosition.y = maxY;
        }

        if (gameObject.transform.position.y > maxY)
        {
            newPosition.y = minY;
        }

        if (newPosition != gameObject.transform.position)
        {
            gameObject.transform.position = newPosition;
        }

    }

    /// <summary>
    /// Popula os limites de tela
    /// </summary>
    private void PopulateBoundaryLimits()
    {  
        minX = gamemanager.GetBottomCorner().x - sprite.bounds.extents.x;
        maxX = gamemanager.GetTopCorner().x + sprite.bounds.extents.x;

        minY = gamemanager.GetBottomCorner().y - sprite.bounds.extents.y;
        maxY = gamemanager.GetTopCorner().y + sprite.bounds.extents.y;
    }
    #endregion
    /// <summary>
    /// Atirar
    /// </summary>
    public virtual void NormalShoot()
    {
        NormalShoot(gameObject.transform);
    }
    /// <summary>
    /// Atirar utilizado pela IA
    /// </summary>
    /// <param name="_ref"></param>
    public virtual void NormalShoot(Transform _ref)
    {
        Bullet _bullet = (Bullet)bulletPool.Instantiate(PrefabsEnum.bullet, typeof(Bullet));

        _bullet.transform.position = _ref.position;
        _bullet.transform.rotation = _ref.rotation;

        _bullet.SetRecycle(bulletPool, PrefabsEnum.bullet);
        _bullet.InitializeBullet(shipType, gamemanager);

        FireOnShipShoot();
    }

    private void FireOnShipShoot()
    {
        if (OnShipShoot != null)
        {
            OnShipShoot();
        }
    }

    void OnDestroy()
    {
        gamemanager.OnGameStart -= gamemanager_OnGameStart;
    }
}
