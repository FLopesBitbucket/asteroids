﻿using UnityEngine;

interface IBoundary
{
    //screen limits
    float minX { get; set; }
    float maxX { get; set; }

    float minY { get; set; }
    float maxY { get; set; }

    SpriteRenderer sprite { get; set; }

    void CheckOutsideLimits();
}
