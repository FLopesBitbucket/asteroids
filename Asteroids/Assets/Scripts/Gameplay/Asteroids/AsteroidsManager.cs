﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsManager : MonoBehaviour {

    [Header("General References")]
    public ObjectPoolManager asteroidPool;
    public GameManager gamemanager;

    [Header("Asteroids Parameters")]
    public int initAsteroidsValue = 2;
    public int increaseAsteroidsValue = 2;
    private int activeAsteroids;

    [Header("Score Parameters")]
    public int scoreBigAsteroid;
    public int scoreMediumAsteroid;
    public int scoreSmallAsteroid;

    private int maxAsteroids = 10;

    public float minYSpawn;
    public float maxYSpawn;
    public float minXSpawn;
    public float maxXSpawn;


	void Start () 
    {
        gamemanager.OnNewLevel += GenerateNewLevel;
        gamemanager.OnDestroyAsteroid += DestroyAsteroid;

        if (FlowControl.Instance.GetGameMode() == GameModes.advanced)
        {
            initAsteroidsValue = 6;
            maxAsteroids = 20;
        }
	}
    
    /// <summary>
    /// Gerar novo nivel baseado no nivel atual
    /// </summary>
    void GenerateNewLevel()
    {
        int _asteroidsCount = initAsteroidsValue + (increaseAsteroidsValue * gamemanager.GetActualLevel());

        if (_asteroidsCount > maxAsteroids)
        {
            _asteroidsCount = maxAsteroids;
        }

        for (int i = 0; i < _asteroidsCount; i++)
        {
            float _spawnPosX = Random.Range(gamemanager.GetBottomCorner().x, gamemanager.GetTopCorner().x);
            float _spawnPosY = Random.Range(gamemanager.GetBottomCorner().y, gamemanager.GetTopCorner().y);

            if (_spawnPosX < maxXSpawn && _spawnPosX > minXSpawn)
            {
                _spawnPosX = maxXSpawn;
            }
            else if (_spawnPosY < maxYSpawn && _spawnPosY > minYSpawn)
            {
                _spawnPosY = maxYSpawn;
            }

            InstantiateAsteroid(PrefabsEnum.BigAsteroid, new Vector3(_spawnPosX, _spawnPosY), 1);  
        }

        CheckActiveAsteroids();

    }

    /// <summary>
    /// Instancia e inicializa asteroid
    /// </summary>
    /// <param name="_type"></param>
    /// <param name="_pos"></param>
    /// <param name="_amount"></param>
    void InstantiateAsteroid(PrefabsEnum _type, Vector3 _pos, int _amount)
    {
        for (int i = 0; i < _amount; i++)
        {
            AsteroidScript _asteroid = (AsteroidScript)asteroidPool.Instantiate(_type, _pos, Quaternion.identity, typeof(AsteroidScript));
            _asteroid.PopulateAsteroid(gamemanager);
            activeAsteroids++;
        }
    }

    /// <summary>
    /// Recicla asteroid, adiciona pontos, instancia asteroids menores caso exista a possibilidade
    /// </summary>
    /// <param name="_asteroidObj"></param>
    /// <param name="_point"></param>
    void DestroyAsteroid(GameObject _asteroidObj, Vector2 _point)
    {
        activeAsteroids--;

        AsteroidScript _asteroidScript = _asteroidObj.GetComponent<AsteroidScript>();

        switch (_asteroidScript.asteroidType)
        {
            case AsteroidsType.Big:
                RecycleAsteroid(PrefabsEnum.BigAsteroid, _asteroidObj);
                InstantiateAsteroid(PrefabsEnum.MediumAsteroid, _asteroidObj.transform.position, 2);

                gamemanager.UpdateScore(scoreBigAsteroid);
                break;
            case AsteroidsType.Medium:
                RecycleAsteroid(PrefabsEnum.MediumAsteroid, _asteroidObj);
                InstantiateAsteroid(PrefabsEnum.SmallAsteroid, _asteroidObj.transform.position, 2);

                gamemanager.UpdateScore(scoreMediumAsteroid);
                break;
            case AsteroidsType.Small:
                RecycleAsteroid(PrefabsEnum.SmallAsteroid, _asteroidObj);

                gamemanager.UpdateScore(scoreSmallAsteroid);
                break;
            default:
                break;
        }
        CheckActiveAsteroids();

    }

    /// <summary>
    /// Verifica a quantidade de asteroids na tela
    /// </summary>
    void CheckActiveAsteroids()
    {
        gamemanager.CheckActiveAsteroids(activeAsteroids);
    }

    /// <summary>
    /// recicla asteroids
    /// </summary>
    /// <param name="_prefab"></param>
    /// <param name="_asteroid"></param>
    void RecycleAsteroid(PrefabsEnum _prefab, GameObject _asteroid)
    {
        asteroidPool.RecicleObject(_prefab, _asteroid);
    }
      
    void OnDestroy()
    {
        gamemanager.OnNewLevel -= GenerateNewLevel;
        gamemanager.OnDestroyAsteroid -= DestroyAsteroid;
    }
	
}
