﻿using UnityEngine;

public class AsteroidScript : MonoBehaviour, IBoundary {

    public ObjectType objType;
    public AsteroidsType asteroidType;
    public float speed;

    private Vector3 orientation;
    private bool initialized;

    private GameManager gamemanager;

    //boundary limits
    [HideInInspector]
    public float minX { get; set; }
    [HideInInspector]
    public float maxX { get; set; }
    [HideInInspector]
    public float minY { get; set; }
    [HideInInspector]
    public float maxY { get; set; }
    [HideInInspector]
    public SpriteRenderer sprite { get; set; }

	void Awake () 
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
	}

    /// <summary>
    /// Inicializa asteroid
    /// </summary>
    /// <param name="_gamemanager"></param>
    public void PopulateAsteroid(GameManager _gamemanager)
    {
        if (gamemanager == null)
            gamemanager = _gamemanager;

        if (FlowControl.Instance.GetGameMode() == GameModes.advanced)
        {
            speed *= 2;
        }

        PopulateBoundaryLimits();

        orientation = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f));

        initialized = true;
    }

    #region Collision Stuffs

    /// <summary>
    /// controla colisões
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.contacts.Length > 0)
        {
            ContactPoint2D contact = collision.contacts[0];

            ObjectType _collideType = (ObjectType)System.Enum.Parse(typeof(ObjectType), contact.collider.tag);


            if (_collideType == ObjectType.shipPlayer)
            {
                gamemanager.Collision(objType, _collideType, contact.point, gameObject, contact.collider.gameObject);
            }
        }
    }

    #endregion
	

    void Update () 
    {
        if (!initialized || gamemanager.GetGameState() != GameState.playing)
            return;

        float _stepFwd = Time.deltaTime * speed;

        gameObject.transform.Translate(orientation * _stepFwd);

        CheckOutsideLimits();
	}

    void OnDisable()
    {
        initialized = false;
    }

    #region Boundary Stuffs
    /// <summary>
    /// Verifica os limites da tela e aplica a nova posição caso o gameObject sai de visao
    /// </summary>
    public virtual void CheckOutsideLimits()
    {
        Vector3 newPosition = gameObject.transform.position;

        if (gameObject.transform.position.x < minX)
        {
            newPosition.x = maxX;
        }

        if (gameObject.transform.position.x > maxX)
        {
            newPosition.x = minX;
        }

        if (gameObject.transform.position.y < minY)
        {
            newPosition.y = maxY;
        }

        if (gameObject.transform.position.y > maxY)
        {
            newPosition.y = minY;
        }

        if (newPosition != gameObject.transform.position)
        {
            gameObject.transform.position = newPosition;
        }

    }

    /// <summary>
    /// Popula os limites de tela
    /// </summary>
    private void PopulateBoundaryLimits()
    {
        minX = gamemanager.GetBottomCorner().x - sprite.bounds.extents.x;
        maxX = gamemanager.GetTopCorner().x + sprite.bounds.extents.x;

        minY = gamemanager.GetBottomCorner().y - sprite.bounds.extents.y;
        maxY = gamemanager.GetTopCorner().y + sprite.bounds.extents.y;
    }
    #endregion

}
