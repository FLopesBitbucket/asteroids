﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IBoundary
{
    private ObjectType objType;

    private ShipType bulletOwner;
    private bool initialized;

    public float bulletSpeed;

    private ObjectPoolManager poolRef;
    private PrefabsEnum prefabEnumRef;
    private GameManager gamemanager;

    //boundary limits
    [HideInInspector]
    public float minX { get; set; }
    [HideInInspector]
    public float maxX { get; set; }
    [HideInInspector]
    public float minY { get; set; }
    [HideInInspector]
    public float maxY { get; set; }
    [HideInInspector]
    public SpriteRenderer sprite { get; set; }

    /// <summary>
    /// Inicializa projetil
    /// </summary>
    /// <param name="_bulletOwner"></param>
    /// <param name="_gamemanager"></param>
    public void InitializeBullet(ShipType _bulletOwner, GameManager _gamemanager)
    {
        if (gamemanager == null)
            gamemanager = _gamemanager;

        bulletOwner = _bulletOwner;

        if (bulletOwner == ShipType.playerShip)
        {
            objType = ObjectType.bulletPlayer;
        }
        else
        {
            objType = ObjectType.bulletEnemy;
        }

        PopulateBoundaryLimits();

        initialized = true;
    }

    /// <summary>
    /// Pega referencias que serao utilizadas ao reciclar
    /// </summary>
    /// <param name="_poolRef"></param>
    /// <param name="_prefabEnumRef"></param>
    public void SetRecycle(ObjectPoolManager _poolRef, PrefabsEnum _prefabEnumRef)
    {
        poolRef = _poolRef;
        prefabEnumRef = _prefabEnumRef;
    }

    /// <summary>
    /// recicla components
    /// </summary>
    void RecycleComponent()
    {
        poolRef.RecicleObject(prefabEnumRef, this.gameObject);
    }

    #region Collision Stuffs

    /// <summary>
    /// controla colisoes
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.contacts.Length > 0)
        {
            ContactPoint2D contact = collision.contacts[0];

            ObjectType _collideType = (ObjectType)Enum.Parse(typeof(ObjectType), contact.collider.tag);

            //print("objType " + objType);
            //print("_collideType " + _collideType);
                    
            gamemanager.Collision(objType, _collideType, contact.point, gameObject, contact.collider.gameObject);

            //if ((objType != ObjectType.bulletPlayer &&  _collideType != ObjectType.shipPlayer) && (objType != ObjectType.bulletEnemy && _collideType != ObjectType.shipEnemy))
            //{
            //    RecycleComponent();
            //}

            if (_collideType == ObjectType.asteroid || (_collideType == ObjectType.shipPlayer && objType == ObjectType.bulletEnemy) || (_collideType == ObjectType.shipEnemy && objType == ObjectType.bulletPlayer))
            {
                RecycleComponent();
            }
        }
    }

    #endregion

    #region Boundary Stuffs
    /// <summary>
    /// Verifica os limites da tela e aplica a nova posição caso o gameObject sai de visao
    /// </summary>
    public virtual void CheckOutsideLimits()
    {
        if (gameObject.transform.position.x < minX || 
            gameObject.transform.position.x > maxX ||
            gameObject.transform.position.y < minY ||
            gameObject.transform.position.y > maxY)
        {
            RecycleComponent();
        }      

    }

    /// <summary>
    /// Popula os limites de tela
    /// </summary>
    private void PopulateBoundaryLimits()
    {
        minX = gamemanager.GetBottomCorner().x;
        maxX = gamemanager.GetTopCorner().x;

        minY = gamemanager.GetBottomCorner().y;
        maxY = gamemanager.GetTopCorner().y;
    }
    #endregion

    void Update () 
    {
        if (!initialized || gamemanager.GetGameState() != GameState.playing)
            return;

        float _stepFwd = Time.deltaTime * bulletSpeed;

        gameObject.transform.Translate(Vector3.up * _stepFwd);

        CheckOutsideLimits();
	}

    void OnDisable()
    {
        initialized = false;
    }

}
