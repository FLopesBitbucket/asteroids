﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupScript : MonoBehaviour {

    private GameManager gamemanager;
    private ObjectPoolManager pool;

    public SpriteRenderer sprite;
    public bool spriteColorChange;

    public ObjectType objType;

    public readonly string s_shield = "shield";
    
    /// <summary>
    /// Inicializa powerups
    /// </summary>
    /// <param name="_gamemanager"></param>
    /// <param name="_pool"></param>
    public void Populate(GameManager _gamemanager, ObjectPoolManager _pool)
    {
        if (gamemanager == null)
            gamemanager = _gamemanager;

        if (pool == null)
            pool = _pool;

        InitializeColors();
    }

    /// <summary>
    /// seta cor caso haja a possibilidade
    /// </summary>
    void InitializeColors()
    {
        if (spriteColorChange)
        {
            if (objType == ObjectType.shield)
            {
                sprite.sprite = gamemanager.colorAtlas[(int)FlowControl.Instance.GetGamePressets()].GetSprite(s_shield);
            }
        }
    }

    /// <summary>
    /// Se auto recicla
    /// </summary>
    void SelfRecycle()
    {
        if (objType == ObjectType.shield)
	    {
            pool.RecicleObject(PrefabsEnum.PowerupShield, this.gameObject);
	    }
    }

    #region Collision Stuffs

    /// <summary>
    /// controla colisoes
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.contacts.Length > 0)
        {
            ContactPoint2D contact = collision.contacts[0];

            ObjectType _collideType = (ObjectType)System.Enum.Parse(typeof(ObjectType), contact.collider.tag);

            if (_collideType == ObjectType.shipPlayer)
            {
                gamemanager.Collision(objType, _collideType, contact.point, gameObject, contact.collider.gameObject);
                SelfRecycle();
            }
        }
    }

    #endregion
}
