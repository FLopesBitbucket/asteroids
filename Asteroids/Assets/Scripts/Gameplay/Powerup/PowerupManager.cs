﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {

    [Header("General References")]
    public ObjectPoolManager powerupPool;
    public GameManager gamemanager;
    private bool gameStarted = false;

    [Header("Shield Parameters")]
    public bool spawnedShield = false;
    public float timeToSpawnShield = 10f;
    private float timeShield;
    void Start()
    {
        gamemanager.OnNewLevel += InitializePowerups;
        gamemanager.OnBreakShield += StartCountdownShield;

        if (FlowControl.Instance.GetGameMode() == GameModes.advanced)
        {
            timeToSpawnShield = 5f;
        }
    }

    /// <summary>
    /// inicializa variaveis de controle sobre powerups
    /// </summary>
    void InitializePowerups()
    {
        gameStarted = true;

        timeShield = timeToSpawnShield;
    }

    void Update()
    {
        if (!gameStarted)
            return;

        if (!spawnedShield)
        {
            timeShield -= Time.deltaTime;

            if (timeShield <= 0)
            {
                SpawnPowerups();
            }
        }
    }

    /// <summary>
    /// Seta contador para spawnar shields
    /// </summary>
    void StartCountdownShield()
    {
        spawnedShield = false;
        timeShield = timeToSpawnShield;
    }

    /// <summary>
    /// Spawn powerup, presetado no momento para ser shield
    /// </summary>
    void SpawnPowerups()
    {
        spawnedShield = true;

        float _spawnPosX = Random.Range(gamemanager.GetBottomCorner().x, gamemanager.GetTopCorner().x);
        float _spawnPosY = Random.Range(gamemanager.GetBottomCorner().y, gamemanager.GetTopCorner().y);

        PowerupScript _shield = (PowerupScript)powerupPool.Instantiate(PrefabsEnum.PowerupShield, new Vector3(_spawnPosX, _spawnPosY), Quaternion.identity, typeof(PowerupScript));
        _shield.Populate(gamemanager, powerupPool);
    }

    void OnDestroy()
    {
        gamemanager.OnNewLevel -= InitializePowerups;
        gamemanager.OnBreakShield -= StartCountdownShield;
    }
}
